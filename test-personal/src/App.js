import "./App.css";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Login from "./components/Login/Login";
import NotFound from "./components/NotFound/NotFound";
import AppContainer from "./hoc/AppContainer";

function App() {
  return (
    <BrowserRouter>
      <AppContainer>
        <div className="App">
          <h1>Sign Translate Page</h1>
        </div>
      </AppContainer>
      <Switch>
        <Route path="/" exact component={Login}></Route>
        <Route path="*" component={NotFound}></Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
