import { useState } from "react";
import AppContainer from "../../hoc/AppContainer";

const Login = () => {
  const [credentials, setCredentials] = useState({
    username: "",
    password: "",
  });

  const onInputChange = (event) => {
    setCredentials({
      ...credentials,
      [event.target.id]: event.target.value,
    });
  };

  return (
    <AppContainer>
      <form className="mt-3">
        <h1>Login 🐲🥐👾</h1>
        <p>WAAAAAAAAAAAAAAAAAAAAAHHH!</p>

        <div className="mb-3">
          <label htmlFor="username" className="form-label">
            Username
          </label>
          <input
            id="username"
            type="text"
            placeholder="Enter your username🥖"
            className="form-control"
            onChange={onInputChange}
          ></input>
        </div>

        <div className="mb-3">
          <label htmlFor="password" className="form-label">
            Password
          </label>
          <input
            id="password"
            type="password"
            placeholder="Enter your pswd 🦹‍♀️"
            className="form-control"
            onChange={onInputChange}
          ></input>
        </div>

        <button type="submit" className="btn btn-primary btn-lg">
          LETSAA GGOOOOOOOOOOOOOOOHH
        </button>
      </form>
    </AppContainer>
  );
};

export default Login;
