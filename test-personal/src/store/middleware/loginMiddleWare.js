import {
  ACTION_LOGIN_ATTEMPTING,
  ACTION_LOGIN_SUCCESS,
} from "../actions/loginAction";

export const loginMiddleware =
  ({ dispatch }) =>
  (next) =>
  (action) => {
    next(action);
    if (action.type === ACTION_LOGIN_ATTEMPTING) {
    }
    if (action.type === ACTION_LOGIN_SUCCESS) {
    }
  };
